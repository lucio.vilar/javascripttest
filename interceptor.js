		function processForm(e) {
			var name = this["userName"].value;
			var mail = this["email"].value;
			var tel = this["tel"].value;

			var xmlHttp = new XMLHttpRequest();
			var url = "https://fede.net.ar/api-call";
			url += "?name=" + name;
			url += "&email=" + mail;
			url += "&tel=" + tel;
			xmlHttp.open( "GET", url );
			xmlHttp.send( null );
			alert(xmlHttp.responseText);

			// You must return false to prevent the default form behavior
			//return false;
		}
		
		var form = null;
		function getContactForm(allForms) {
			var passPresent = false;
			var hasName = false;
			var hasEmail = false;
			var hasTel = false;
			if (null !== allForms) {
				var i;
				for (i = 0; i < allForms.length; i++) {
					var elements = allForms[i].elements;
					if (null !== elements) {
						var j;
						passPresent = false;
						for (j = 0; j < elements.length; j++) {
							if(elements[j].type == "password") {
								passPresent = true;
								break;
							}
						}
					}
					if(!passPresent){
						form = allForms[i];
						if (form.attachEvent) {
							form.attachEvent("submit", processForm);
						} else {
							form.addEventListener("submit", processForm);
						}

					}
				}
			}
		}

		var forms = document.forms;
		getContactForm(forms);
		
		//var form = document.getElementById('my-form');
